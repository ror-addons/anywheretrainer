Title: AnywhereTrainer v1.3

Author: DarthVon (thedpui02@sneakemail.com) - Original author
				Alpha_Male (alpha_male@speakeasy.net) - Current maintainer

Description: Allows quick and easy access to all the training windows without having to be near the NPC trainers.

						 Simply open the Character Window, there will be 4 new buttons on the right side. Hover over each 
						 button to see what it does.

Features: * Allows access to the following training interfaces:
						* Core Training
						* Mastery Training
						* Renown Training
						* Tome Training 
					* Integrates into the Character Window seamlessly
					* Zero configuration required 

Files: \source\AnywhereTrainer.lua
			 \source\AnywhereTrainer.xml
			 \AnywhereTrainer.mod
			 \AnywhereTrainer_Install.txt
			 \AnywhereTrainer_Readme.txt

Version History: 1.0 - Initial Release
									 1.1 - New Features and Fixes
											 - Added support for 1.3.1 WARInfo Categories and Careers (AnywhereTrainer.mod)
											 - Added support for version info (AnywhereTrainer.mod)
											 - Updated mod version information for 1.4.0
											 - AnywhereTrainer window position fix (1.4.0 Character Window changes)
											 - Directory structure reorganization
											 - Readme and install file additions
									 1.2 - New Features and Maintenance Update
											 - Updated mod version information for 1.4.1
											 - Added support for AdvancedRenownTrainer
											   (http://war.curse.com/downloads/war-addons/details/advancedrenowntrainer.aspx)
									 1.3 - Maintenance Update
											 - Updated mod version information for 1.4.5

Supported Versions: Warhammer Online v1.4.5

Dependencies: None

Addon Compatability: Compatible with:
										 - Character View Expanded Stats (CaVES) by Alpha_Male (alpha_male@speakeasy.net)
										 - Advanced Renown Trainer by Varonth

Future Features: None currently

Known Issues: The game requires you to be near the specific NPC to actually purchase new abilities, so you 
							cannot actually use the windows made available by AnywhereTrainer to train anywhere. You can 
							however view all the details of what you have and what is available.

Additional Credits: Varonth for his collaboration and changes to make AdvancedRenownTrainer and
										AnywhereTrainer compatible

Special Thanks:	EA/Mythic for a great game and for releasing the API specs
							  The War Wiki (www.thewarwiki.com) for being a great source of knowledge for WAR mod development
								www.curse.com and www.curseforge.com for hosting WAR mod files and projects
								Ominous Latin Name guild on Gorfang for testing and feedback
								Trouble guild on Gorfang for their support

License/Disclaimers: This addon and it's relevant parts and files are released under the 
										 original author's MIT License.

Additional Notes:  The above information about the addon can also be found in the comment header in the .lua files 
(\source\AnywhereTrainer.lua.

For additional help and support with AnywhereTrainer please visit http://war.curse.com/downloads/war-addons/details/AnywhereTrainer.aspx